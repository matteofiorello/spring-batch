package com.job.runner;

import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.launch.JobInstanceAlreadyExistsException;
import org.springframework.batch.core.launch.JobOperator;
import org.springframework.batch.core.launch.NoSuchJobException;

public interface IJMXJobRunner {

	void runJob(String name, String parameter) throws NoSuchJobException, JobInstanceAlreadyExistsException, JobParametersInvalidException;

	void setOperator(JobOperator operator);

}
