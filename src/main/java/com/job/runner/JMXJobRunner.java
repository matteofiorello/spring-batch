package com.job.runner;

import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.launch.JobInstanceAlreadyExistsException;
import org.springframework.batch.core.launch.JobOperator;
import org.springframework.batch.core.launch.NoSuchJobException;

//used as MBEAN
public class JMXJobRunner implements IJMXJobRunner {
	private JobOperator operator;

	@Override
	public void runJob(String name, String parameter) throws NoSuchJobException, JobInstanceAlreadyExistsException, JobParametersInvalidException {
		operator.start(name, parameter);
	}

	@Override
	public void setOperator(JobOperator operator) {
		this.operator = operator;
	}
}