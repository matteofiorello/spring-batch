package com.bean.mapper.row;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.bean.AccountSummary;

public class AccountSummaryRowMapper implements RowMapper<AccountSummary> {

	@Override
	public AccountSummary mapRow(ResultSet resulSet, int paramInt) throws SQLException {
		AccountSummary summary = new AccountSummary();
		summary.setAccountNumber(resulSet.getString(1));
		summary.setCurrentBalance(resulSet.getDouble(2));
		return summary;
	}

}
