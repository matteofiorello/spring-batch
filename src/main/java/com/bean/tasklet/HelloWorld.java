package com.bean.tasklet;

import java.util.List;
import java.util.Map;

import org.springframework.batch.core.JobInstance;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.explore.JobExplorer;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.scope.context.StepContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.repeat.RepeatStatus;

public class HelloWorld implements Tasklet {
	private static final String HELLO_WORLD = "Hello, world!";
	private static final String HELLO_WORLD_PARAMETER = "Hello, %s";
	private static final String JOB_NAME = "helloWorldJob";
	private static final String WELCOME = "Welcome back %s!";

	private String name;
	private JobExplorer explorer;

	public RepeatStatus execute(StepContribution arg0, ChunkContext context) throws Exception {
		System.out.println(HELLO_WORLD);
		StepContext stepContext = context.getStepContext();
		Map<String, Object> jobParameters = stepContext.getJobParameters();
		putParameterExecutionContext(stepContext);
		usingJobExplorer();
		retrieveJobParameters(jobParameters);
		// Using spring late binding
		System.out.println(String.format(HELLO_WORLD_PARAMETER + " from spring", this.getName()));

		return RepeatStatus.FINISHED;
	}

	private void usingJobExplorer() {
		List<JobInstance> instances = explorer.getJobInstances(JOB_NAME, 0, Integer.MAX_VALUE);
		System.out.println("instances of job " + instances);
		if (instances != null && instances.size() > 1) {
			System.out.println(String.format(WELCOME, name));
		} else {
			System.out.println(String.format(HELLO_WORLD_PARAMETER, name));
		}
	}

	private void retrieveJobParameters(Map<String, Object> jobParameters) {
		if (jobParameters.containsKey("name")) {
			String name = (String) jobParameters.get("name");
			System.out.println(String.format(HELLO_WORLD_PARAMETER, name));
		}
	}

	private void putParameterExecutionContext(StepContext stepContext) {
		ExecutionContext executionContext = stepContext.getStepExecution().getExecutionContext();
		executionContext.put("user.name", name);
		stepContext.getStepExecution().getJobExecution().getExecutionContext().put("jobKey", name + "2");
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public JobExplorer getExplorer() {
		return explorer;
	}

	public void setExplorer(JobExplorer explorer) {
		this.explorer = explorer;
	}

}
