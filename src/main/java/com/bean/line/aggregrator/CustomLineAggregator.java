package com.bean.line.aggregrator;

import org.springframework.batch.item.file.transform.LineAggregator;

import com.bean.Customer;
import com.bean.Transaction;

public class CustomLineAggregator implements LineAggregator<Object> {
	private LineAggregator<Customer> customerLineAggregator;
	private LineAggregator<Transaction> transactionLineAggregator;

	public String aggregate(Object record) {
		if (record instanceof Customer) {
			return customerLineAggregator.aggregate((Customer) record);
		} else {
			return transactionLineAggregator.aggregate((Transaction) record);
		}
	}

	public void setCustomerLineAggregator(LineAggregator<Customer> customerLineAggregator) {
		this.customerLineAggregator = customerLineAggregator;
	}

	public void setTransactionLineAggregator(LineAggregator<Transaction> transactionLineAggregator) {
		this.transactionLineAggregator = transactionLineAggregator;
	}
}