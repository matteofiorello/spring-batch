package com.bean.item.listener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.listener.ItemListenerSupport;
import org.springframework.batch.item.file.FlatFileParseException;

import com.bean.Customer;

public class CustomerItemListener extends ItemListenerSupport<Customer, Customer> {
	private static final Logger LOGGER = LoggerFactory.getLogger(CustomerItemListener.class);

	@Override
	public void onReadError(Exception e) {
		if (e instanceof FlatFileParseException) {
			FlatFileParseException ffpe = (FlatFileParseException) e;
			StringBuilder errorMessage = new StringBuilder();
			errorMessage.append("An error occured while processing the " + ffpe.getLineNumber() + " line of the file. Below was the faulty " + "input.\n");
			errorMessage.append(ffpe.getInput() + "\n");
			LOGGER.error(errorMessage.toString(), ffpe);
		} else {
			LOGGER.error("An error has occured", e);
		}
	}
}