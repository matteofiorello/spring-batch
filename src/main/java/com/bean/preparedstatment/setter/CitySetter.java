package com.bean.preparedstatment.setter;

import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.springframework.jdbc.core.PreparedStatementSetter;

public class CitySetter implements PreparedStatementSetter {
	private String city;

	public void setValues(PreparedStatement ps) throws SQLException {
		ps.setString(1, city);
	}

	public void setCity(String city) {
		this.city = city;
	}
}