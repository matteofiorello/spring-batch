package com.bean.processor;

import java.util.List;

import org.springframework.batch.item.ItemProcessor;

import com.bean.AccountSummary;
import com.bean.Transaction;
import com.dao.TransactionDao;

public class TransactionApplierProcessor implements ItemProcessor<AccountSummary, AccountSummary> {

	private TransactionDao transactionDao;

	public AccountSummary process(AccountSummary summary) throws Exception {
		List<Transaction> transactions = transactionDao.getTransactionsByAccountNumber(summary.getAccountNumber());
		for (Transaction transaction : transactions) {
			summary.setCurrentBalance(summary.getCurrentBalance() + transaction.getAmount());
		}
		return summary;
	}

	public void setTransactionDao(TransactionDao transactionDao) {
		this.transactionDao = transactionDao;
	}
}