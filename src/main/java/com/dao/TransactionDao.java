package com.dao;

import java.util.List;

import com.bean.Transaction;

public interface TransactionDao {

	List<Transaction> getTransactionsByAccountNumber(String accountNumber);

}
