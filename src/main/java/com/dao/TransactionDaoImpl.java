package com.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import com.bean.Transaction;

public class TransactionDaoImpl extends JdbcTemplate implements TransactionDao {

	@SuppressWarnings("unchecked")
	@Override
	public List<Transaction> getTransactionsByAccountNumber(String accountNumber) {
		RowMapper rowMapper = new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				Transaction trans = new Transaction();
				trans.setAmount(rs.getDouble("amount"));
				trans.setTimestamp(rs.getDate("timestamp"));
				return trans;
			}
		};

		// RowMapper rowMapper2 = (ResultSet rs, int rowNum) -> {
		// Transaction trans = new Transaction();
		// trans.setAmount(rs.getDouble("amount"));
		// trans.setTimestamp(rs.getDate("timestamp"));
		// return trans;
		// };

		return query("select t.id, t.timestamp, t.amount from transaction t " + "inner join account_summary a on a.id = t.account_summary_"
				+ "id where a.account_number = ?", new Object[] { accountNumber }, rowMapper);
	}
}
