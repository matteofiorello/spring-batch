package com.data.generator;

import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class TransactionGenerator {

	@SuppressWarnings("unused")
	public static void main(String[] args) throws IOException {
		List<String> accounts = Arrays.asList("3985729381", "1234567", "7654321");
		try (FileWriter file = new FileWriter("C://progetti//GLCRA//springBatchAdminTC//src//main//resources//csv//transaction.csv");) {
			file.write("3985729387,2010-01-08 12:15:26,523.65\n");
			int i = 0;
			Random n = new Random();
			while (i < 33000) {
				for (String account : accounts) {
					int nextDouble = n.nextInt();

					file.write(account + ",2010-01-08 12:15:26," + Math.round(nextDouble) / 10000 + "\n");
				}
				i++;
			}
			file.write("100000");

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
